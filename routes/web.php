<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'auth.login');

Auth::routes(['register' => false]);

Route::get('home', 'HomeController@index')->name('home');

Route::resource('users', 'UsersController');

Route::resource('entreprises', 'EntreprisesController');

Route::resource('offres', 'OffresController');
Route::post('offres/download/{offre}', 'OffresController@download')->name('offres.download');