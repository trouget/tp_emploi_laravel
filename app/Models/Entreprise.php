<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $fillable = ['nom', 'addresse', 'code_postal', 'ville', 'contact', 'email', 'telephone'];

    protected $primaryKey = 'id_entreprise';

    public function offres()
    {
        return $this->hasMany('App\Models\Offre', 'id_entreprise', 'id_entreprise');
    }
}
