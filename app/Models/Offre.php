<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $fillable = ['titre', 'description', 'niveau', 'pdf', 'id_entreprise'];

    protected $primaryKey = 'id_offre';

    public function entreprise()
    {
        return $this->belongsTo('App\Models\Entreprise', 'id_entreprise', 'id_entreprise');

    }
}
