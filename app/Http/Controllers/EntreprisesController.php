<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Entreprise;

class EntreprisesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entreprises = Entreprise::all();

        return view('entreprises.indexEntreprise', compact('entreprises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entreprises.createEntreprise');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Entreprise::create($this->validateData());

        return redirect()->route('entreprises.index')->withToastSuccess('Entreprise créée avec succès !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Entreprise $entreprise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Entreprise $entreprise)
    {
        return view('entreprises.editEntreprise', compact('entreprise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entreprise $entreprise)
    {
        $entreprise->update($this->validateData());

        return redirect()->route('entreprises.index')->withToastSuccess('Entreprise modifiée avec succès !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entreprise $entreprise)
    {
        $entreprise->delete();

        return redirect()->route('entreprises.index')->withToastSuccess('Entreprise supprimée avec succès !!');
    }

    private function validateData()
    {
        return \Validator::make(request()->all(), [
                'nom' => 'required|string|max:255',
                'email' => 'required|email|max:255',
                'addresse' => 'required|string|max:255',
                'code_postal' => 'required|integer|digits:5',
                'ville' => 'required|string|max:255',
                'contact' => 'required|string|max:255',
                'telephone' => 'required|string|size:14',
            ])->validate();
    }
}
