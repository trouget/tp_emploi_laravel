<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use \App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request)
    {
        if (User::where('email', $request->email)->first() == null)
        {
           return back()->withErrors(['email' => 'L\'email n\'est pas renseigner !! ']);
        }

        $user = User::where('email', $request->email)->first();

        $request->validate([
            'email' => 'required|email|unique:users,id,'.$user->id.'|max:255',
            'password' => 'required|string|min:8|confirmed',
        ]);

       return $this->updatePass($user);
    }

    private function updatePass(User $user)
    {
        if(request('password'))
        {
            $user->update([
                'password' => \Hash::make(request()->get('password'))
            ]);

            return redirect('/')->withToastSuccess('Mot de passe rénitialiser avec succès !!');;
        }
    }
}
