<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\Offre;
use \App\Models\Entreprise;

class OffresController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['show','index', 'download']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->is_admin == true)
        {
            $offres = Offre::all();

            return view('offres.indexOffre', compact('offres'));
        }
        else
        {
            $offres = Offre::paginate(5);

            return view('offres.viewOffre', compact('offres'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entreprises = Entreprise::all();

        return view('offres.createOffre', compact('entreprises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $offre = Offre::create($this->validateData());

        $this->storeFile($offre);

        return redirect()->route('offres.index')->withToastSuccess('Offre créée avec succès !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        return view('offres.showOffre', compact('offre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Offre $offre)
    {
        $entreprises = Entreprise::all();
        return view('offres.editOffre', compact('offre', 'entreprises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offre $offre)
    {
        $offre->update($this->validateData());

        $this->storeFile($offre);

        return redirect()->route('offres.index')->withToastSuccess('Offre modifiée avec succès !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offre $offre)
    {
        $offre->delete();

        return redirect()->route('offres.index')->withToastSuccess('Offre supprimée avec succès !!');
    }

    public function download(Offre $offre)
    {
        if ($offre->pdf != null)
        {
            return Storage::download($offre->pdf);
        }
        else
        {
            return back()->withToastError('Pas de descriptif pour cette offre d\'emploi !!');
        }
        
    }

    private function storeFile(Offre $offre)
    {
        if(request('pdf'))
        {
            $offre->update([
                'pdf' => request()->file('pdf')->store('pdf')
            ]);
        }
    }

    private function validateData()
    {
        return \Validator::make(request()->all(), [
            'titre' => 'required|string|max:255',
            'description' => 'required|string|min:3',
            'niveau' => 'required|string|max:255',
            'pdf' => 'sometimes|mimes:pdf|max:2048',
            'id_entreprise' => 'required|integer',
        ])->validate();
    }

}
