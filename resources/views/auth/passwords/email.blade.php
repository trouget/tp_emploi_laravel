@extends('layouts.loginTemplate')
@section('title_page', 'Rénitilisation')
@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
<div class="container-login100">
    <div class="wrap-login100">
        <div class="login100-form-title" style="background-image: url({{asset('assets/Login/images/bg-01.jpg')}});">
            <span class="login100-form-title-1">
                PLATOFFRES - Evoyer un lien pour rénitiliser le mot de passe
            </span>
        </div>
        <form class="login100-form validate-form" method="POST" action="{{route('password.email')}}">
            @csrf
            @method('POST')
            <div class="wrap-input100 m-b-26 @error('email') alert-validate @enderror" data-validate="@error('email') {{$errors->first('email')}} @enderror">
                <span class="label-input100">Addresse Email</span>
                <input class="input100" type="text" name="email" id="email" placeholder="Entrer votre adresse mail">
                <span class="focus-input100"></span>
            </div>

            <div class="container-login100-form-btn">
                <button class="login100-form-btn" type="submit">
                    Envoyer le lien de rénitialisation
                </button>
            </div>
        </form>
    </div>
</div>
</div>
    
@endsection