@extends('layouts.loginTemplate')
@section('title_page', 'Réinitilisation')
@section('content')
<div class="container-login100">
    <div class="wrap-login100">
        <div class="login100-form-title" style="background-image: url({{asset('assets/Login/images/bg-01.jpg')}});">
            <span class="login100-form-title-1">
                PLATOFFRES - Réinitialisez votre mot de passe
            </span>
        </div>

        <form class="login100-form validate-form" method="POST" action="{{route('password.update')}}">
            @csrf
            @method('POST')
            <div class="wrap-input100 m-b-26 @error('email') alert-validate @enderror" data-validate="@error('email') {{$errors->first('email')}} @enderror">
                <span class="label-input100">Adresse email</span>
                <input class="input100" type="text" name="email" id="email" placeholder="Entrer votre adresse mail" value="{{ $email ?? old('email') }}">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 m-b-18 @error('password') alert-validate @enderror" data-validate = "@error('password') {{$errors->first('password')}} @enderror">
                <span class="label-input100">Mot de passe</span>
                <input class="input100" type="password" name="password" id="password" placeholder="Entrer votre mot de passe">
                <span class="focus-input100"></span>
            </div>
            
            <div class="wrap-input100 m-b-18">
                <span class="label-input100">Mot de passe</span>
                <input class="input100" type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer le mot de passe">
                <span class="focus-input100"></span>
            </div>

            <div class="container-login100-form-btn" style="margin-top: 20px;">
                <button class="login100-form-btn" type="submit">Réinitialiser</button>
            </div>
        </form>
    </div>
</div>
@endsection