@extends('layouts.loginTemplate')
@section('title_page', 'Connexion')
@section('content')
<div class="container-login100">
	<div class="wrap-login100">
		<div class="login100-form-title" style="background-image: url({{asset('assets/Login/images/bg-01.jpg')}});">
			<span class="login100-form-title-1">
				PLATOFFRES - Connexion
			</span>
		</div>

		<form class="login100-form validate-form" method="POST" action="{{route('login')}}">
			@csrf
			@method('POST')
			<div class="wrap-input100 m-b-26 @error('email') alert-validate @enderror" data-validate="@error('email') {{$errors->first('email')}} @enderror">
				<span class="label-input100">Addresse Email</span>
				<input class="input100" type="text" name="email" id="email" placeholder="Entrer votre adresse mail">
				<span class="focus-input100"></span>
			</div>

			<div class="wrap-input100 m-b-18 @error('password') alert-validate @enderror" data-validate = "@error('password') {{$errors->first('password')}} @enderror">
				<span class="label-input100">Mot de passe</span>
				<input class="input100" type="password" name="password" id="password" placeholder="Entrer votre mot de passe">
				<span class="focus-input100"></span>
			</div>

			<div class="flex-sb-m w-full p-b-30">
				<div class="contact100-form-checkbox">
					<input class="input-checkbox100" id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
					<label class="label-checkbox100" for="remember">
						Se souvenir de moi
					</label>
				</div>
				@if (Route::has('password.request'))
				<div>
					<a href="{{ route('password.request') }}" class="txt1">
						Mot de passe oublié
					</a>
				</div>
				@endif
			</div>

			<div class="container-login100-form-btn">
				<button class="login100-form-btn" type="submit">
					connexion
				</button>
			</div>
		</form>
	</div>
</div>
@endsection