@extends('layouts.template')
@section('title_page', 'Créer un utilisateur')
@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Créer un utilisateur</h3>
    </div>
    <form method="POST" action="{{route('users.store') }}">
        @csrf
        @method('POST')
      <div class="card-body">
          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Nom et prénom</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Entrer un nom et un prénom..." value="{{ old('name') }}">
                    @error('name')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                    @enderror
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Addresse Email </label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Entrer une adresse mail..." value="{{ old('email') }}">
                    @error('email')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                    @enderror
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Entrer un mot de passe...">
                    @error('password')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('password')}}</span>
                    @enderror
                </div>
            </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password_confirmation">Confirmation du mot de passe</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Entrer a nouveau le mot de passe...">
                    </div>
                </div>
        </div>
        <div class="row">
          <div class="col-sm-6" style="margin: auto;">
            <div class="form-group">
              <label for="is_admin">Slectionner un rôle</label>
              <select class="form-control" id="is_admin" name="is_admin">
                <option value="0">Utilisateur</option>
                <option value="1">Administrateur</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Créer l'utilisateur</button>
      </div>
    </form>
    </div>

@endsection