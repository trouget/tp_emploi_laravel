@extends('layouts.template')
@section('title_page', 'Utilisateurs')
@section('content')
      <div class="card card-pink">
        <div class="card-header">
          <h3 class="card-title">Liste des utilisateurs</h3>
        </div>
        <div class="card-body">
            <table id="table" class="table table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Nom et prénom</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Adresse Email</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Rôle</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr role="row" class="odd">                
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>@if ($user->is_admin == true) Administrateur @elseif ($user->is_admin == false) Utilisateur @endif</td>
                    <td>
                        <div class="btn-group" style="display: block;text-align: right;">
                            <a href="{{route('users.edit', $user)}}"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                        <button class="btn btn-danger remove-user" data-name="{{$user->name}}" data-action="{{ route('users.destroy',$user) }}"><i class="fas fa-trash"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @section('script')

      <script>
$("body").on("click",".remove-user",function(){

var current_object = $(this);
var name = current_object.attr('data-name');

swal.fire({

    title: "Êtes vous sur ?",
    text: "Voulez-vous vraiment supprimer l'utilisateur suivant : "+name+" ?",
    type: 'warning',
    showCancelButton: true,
    cancelButtonColor: '#6c757d',
    confirmButtonColor: '#dc3545',
    confirmButtonText: 'Supprimer !',
    reverseButtons: true

}).then((result) =>{

    if (result.value) {

        var action = current_object.attr('data-action');

        $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
        $('body').find('.remove-form').append('@csrf');
        $('body').find('.remove-form').append('@method('DELETE')');
        $('body').find('.remove-form').submit();
    }

});

});

      </script>
          
      @endsection

@endsection
