@extends('layouts.template')
@if (Auth::user() == $user) @section('title_page', 'Mettre à jour mon profil') @else @section('title_page', 'Mise à jour de l\'utilisateur '.$user->name) @endif
@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">@if (Auth::user() == $user) Modifier mon profil @else Modifier l'utilisateur {{$user->name}} @endif</h3>
    </div>
    <form method="POST" action="{{ route('users.update', $user) }}">
        @csrf
        @method('PATCH')
      <div class="card-body">
          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Nom et prénom</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Entrer un nom et un prénom..." value="@if (old('name')) {{old('name')}}@else {{$user->name}}@endif">
                    @error('name')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                    @enderror
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Addresse Email </label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Entrer une adresse mail..." value="@if (old('email')) {{old('email')}}@else {{$user->email}}@endif">
                    @error('email')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                    @enderror
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Entrer un mot de passe...">
                    @error('password')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('password')}}</span>
                    @enderror
                </div>
            </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password_confirmation">Confirmation du mot de passe</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Entrer a nouveau le mot de passe...">
                    </div>
                </div>
        </div>
        @if (Auth::user()->is_admin == true)
        <div class="row">
            <div class="col-sm-6" style="margin: auto;">
              <div class="form-group">
                <label for="is_admin">Slectionner un rôle</label>
                <select class="form-control" id="is_admin" name="is_admin">
                  <option value="0" @if ($user->is_admin == false) selected @endif>Utilisateur</option>
                  <option value="1" @if ($user->is_admin == true) selected @endif>Administrateur</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        @endif
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">@if (Auth::user() == $user) Mettre à jour mon profil @else Mettre à jour l'utilisateur @endif</button>
      </div>
    </div>
    </form>
    </div>

@endsection