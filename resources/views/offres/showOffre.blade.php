@extends('layouts.template')
@section('title_page', 'Offre d\'emploi N°'.$offre->id_offre)
@section('content')
<div class="invoice p-3 mb-3">
  <div class="row">
    <div class="col-12">
      <h1 class="text-primary" style="text-align: center;">{{$offre->titre}}</h1>
    </div>
  </div>
  <div class="row invoice-info" style="margin: 10px; margin-top: 20px;">
    <div class="col-sm-4 invoice-col">
      <h5><i class="fas fa-map-marker-alt fa-lg" style="margin: 10px;" ></i> {{$offre->entreprise->code_postal." ".$offre->entreprise->ville}}</h5>
      <h5><i class="fas fa-building fa-lg" style="margin: 10px;" ></i> {{$offre->entreprise->nom}}</h5>
    </div>
    <div class="col-sm-4 invoice-col">
      <h5><i class="fas fa-address-book fa-lg" style="margin: 10px;" ></i> {{$offre->entreprise->contact}}</h5>
      <h5><i class="fas fa-envelope fa-lg" style="margin: 10px;" ></i> {{$offre->entreprise->email}}</h5>
    </div>
    <div class="col-sm-4 invoice-col">
      <h5><i class="fas fa-phone fa-lg" style="margin: 10px;" ></i> {{$offre->entreprise->telephone}}</h5>
      <h5><i class="fas fa-graduation-cap fa-lg" style="margin: 10px;" ></i> {{$offre->niveau}}</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <h5 class="text-primary" style="margin: 10px;margin-top: 20px;">Description du poste :</h5>
      <h6> {{$offre->description}}</h6>
    </div>
  </div>
  <div class="row">
    <div class="col-12" style="display: block ruby;">
      <h5 class="text-primary" style="margin-top: 20px;margin-right: 10px;">Déscriptif du poste :</h5>
      @if ($offre->pdf != null)
      <form method="POST" action="{{route('offres.download', $offre)}}">
        @csrf
        @method('POST')
        <button type="submit" class="btn btn-block bg-gradient-success btn-sm"><i class="fas fa-file-download" style="margin-right: 5px;"></i>Télécharger le descriptif </button>
      </form>
      @else
      <h6><strong> Pas de descriptif de poste joint a cette annonce</strong></h6>
      @endif
    </div>
  </div>       
  <div class="row">
    <div class="col-3">
      <h5 class="text-primary" style="margin-top: 20px;">Addresse de l'entreprise  :</h5>
      <h6> {{$offre->entreprise->addresse." ".$offre->entreprise->code_postal." ".$offre->entreprise->ville}} </h6>
    </div>
    <div class="col-9">
      <iframe style="margin-top: 20px;" width="100%" height="450" frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?key=AIzaSyALxB0jNB59URaD5sPYLwi0bqyUDyOVjqA&q={{str_replace(" ","+",$offre->entreprise->addresse)."+".$offre->entreprise->code_postal."+".str_replace(" ","+",$offre->entreprise->ville)}}" allowfullscreen ></iframe>
    </div>
  </div>                
</div>



@endsection