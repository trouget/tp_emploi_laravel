@extends('layouts.template')
@section('title_page', 'Les offres d\'emploi')
@section('content')
@foreach($offres as $offre)
<div class="card card-navy card-outline">
    <div class="card-header">
      <h3 class="card-title">
        <a href="{{route('offres.show', $offre->id_offre)}}">{{$offre->titre}}</a>
      </h3>
    </div>
    <div class="card-body">
        <div class="col-12">
            <strong>
                <p class="text-primary" style="display: inline;">Localisation : </p>
            </strong>
            {{$offre->entreprise->code_postal." ".$offre->entreprise->ville}}
        </div>
        <div class="col-12">
            <strong>
                <p class="text-primary" style="display: inline;">Entreprise : </p>
            </strong> 
            {{$offre->entreprise->nom}}
        </div>
        <div class="col-12">
        <strong>
            <p class="text-primary">Description du poste :</p>
        </strong>
        </div>
        {{substr($offre->description, 0, 300)."..."}} <a href="{{route('offres.show', $offre)}}">voir plus</a>
    </div>
    <!-- /.card-body -->
  </div>
  @endforeach

  <div class="row d-flex justify-content-center">
    {{$offres->links()}}
  </div>
  
@endsection