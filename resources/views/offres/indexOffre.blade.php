@extends('layouts.template')
@section('title_page', 'Liste des offres d\'emploi')
@section('content')
      <div class="card card-lightblue">
        <div class="card-header">
          <h3 class="card-title">Liste des offres d'emploi</h3>
        </div>
        <div class="card-body">
            <table id="table" class="table table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Titre</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Descritpion</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">niveau</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Entreprise</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($offres as $offre)
                <tr role="row" class="odd">                
                    <td>{{$offre->titre}}</td>
                    <td>{{substr($offre->description, 0, 50)."..."}} </td>
                    <td>{{$offre->niveau}}</td>
                    <td>{{$offre->entreprise->nom}}</td>
                    <td>
                        <div class="btn-group" style="display: block;text-align: right;">
                            <form method="POST" action="{{route('offres.download', $offre)}}">
                            @csrf
                            @method('POST')
                            <button type="submit" class="btn btn-success"><i class="fas fa-file-download"></i></button>
                            </form>
                            <a href="{{route('offres.edit', $offre)}}"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                        <button class="btn btn-danger remove-user" data-name="{{$offre->titre}}" data-action="{{ route('offres.destroy',$offre) }}"><i class="fas fa-trash"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @section('script')

      <script>
$("body").on("click",".remove-user",function(){

var current_object = $(this);
var name = current_object.attr('data-name');

swal.fire({

    title: "Êtes vous sur ?",
    text: "Voulez-vous vraiment supprimer l'offre d'emploi suivante : "+name+" ?",
    type: 'warning',
    showCancelButton: true,
    cancelButtonColor: '#6c757d',
    confirmButtonColor: '#dc3545',
    confirmButtonText: 'Supprimer !',
    reverseButtons: true

}).then((result) =>{

    if (result.value) {

        var action = current_object.attr('data-action');

        $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
        $('body').find('.remove-form').append('@csrf');
        $('body').find('.remove-form').append('@method('DELETE')');
        $('body').find('.remove-form').submit();
    }

});

});

      </script>
          
      @endsection

@endsection
