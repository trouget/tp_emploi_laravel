@extends('layouts.template')
@section('title_page', 'Créer une offre d\'emploi')
@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Créer une offre d'emploi</h3>
    </div>
    <form method="POST" action="{{route('offres.store') }}" enctype="multipart/form-data">
        @csrf
        @method('POST')
      <div class="card-body">
          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="nom">titre de l'offre d'emploi</label>
                    <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" placeholder="Entrer un titre pour l'offre d'emploi..." value="{{ old('titre') }}">
                    @error('titre')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('titre')}}</span>
                    @enderror
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Niveau d'étude</label>
                    <input type="text" class="form-control @error('niveau') is-invalid @enderror" id="niveau" name="niveau" placeholder="Entrer un niveau d'étude, un diplôme..." value="{{ old('niveau') }}">
                    @error('niveau')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('niveau')}}</span>
                    @enderror
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-6" data-select2-id="69">
                <div class="form-group" data-select2-id="68">
                  <label for="id_entreprise">Selectionner une entreprise</label>
                    <select class="form-control select2bs4 select2-hidden-accessible" style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true" name="id_entreprise" id="id_entreprise">
                        @foreach($entreprises as $entreprise)
                        <option value="{{$entreprise->id_entreprise}}">{{$entreprise->nom}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pdf">Descriptif du poste en PDF (optionel)</label>
                  <div class="input-group">
                    <div class="custom-file" style="display: unset;">
                      <input type="file" class="custom-file-input @error('pdf') is-invalid @enderror" id="pdf" name="pdf">
                      <label class="custom-file-label" for="pdf">Choisir un fichier</label>
                      @error('pdf')
                      <span id="name-error" class="error invalid-feedback">{{$errors->first('pdf')}}</span>
                    @enderror
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="row">
                <label for="description">Description de l'offre d'emploi</label>
                 <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Entrer une description ..." name="description" id="description" value="{{ old('description') }}">{{old('description')}}</textarea>
                @error('description')
                    <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                @enderror
          </div>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Créer l'offre d'emploi</button>
      </div>
    </form>
    </div>

@endsection