@extends('layouts.template')
@section('title_page', 'Modifier une entreprise')
@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Modifier une entreprise</h3>
    </div>
    <form method="POST" action="{{ route('entreprises.update', $entreprise) }}">
        @csrf
        @method('PATCH')
      <div class="card-body">
          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="nom">Nom de l'entreprise</label>
                    <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom" name="nom" placeholder="Entrer un nom pour l'entreprise..." value="@if (old('nom')) {{ old('nom') }} @else {{$entreprise->nom}} @endif">
                    @error('nom')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('nom')}}</span>
                    @enderror
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Addresse Email </label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Entrer une adresse mail..." value="@if (old('email')) {{ old('email') }} @else {{$entreprise->email}} @endif">
                    @error('email')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                    @enderror
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="addresse">Addresse</label>
                    <input type="text" class="form-control @error('addresse') is-invalid @enderror" id="addresse" name="addresse" placeholder="Entrer une addresse..." value="@if (old('addresse')) {{ old('addresse') }} @else {{$entreprise->addresse}} @endif">
                    @error('addresse')
                        <span id="name-error" class="error invalid-feedback">{{$errors->first('addresse')}}</span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="code_postal">Code postale</label>
                    <input type="text" class="form-control @error('code_postal') is-invalid @enderror" id="code_postal" name="code_postal" placeholder="Entrer un code postale ..." value="@if (old('code_postal')) {{ old('code_postal') }} @else {{$entreprise->code_postal}} @endif " data-inputmask="'mask': '99999'">
                    @error('code_postal')
                     <span id="name-error" class="error invalid-feedback">{{$errors->first('code_postal')}}</span>
                    @enderror
                </div>
            </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="ville">Ville</label>
                        <input type="text" class="form-control @error('ville') is-invalid @enderror" id="ville" name="ville" placeholder="Entrer une ville..." value="@if (old('ville')) {{ old('ville') }} @else {{$entreprise->ville}} @endif">
                        @error('ville')
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('ville')}}</span>
                        @enderror
                    </div>
                </div>

        </div>
        <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                  <label for="contact">Contact</label>
                  <input type="text" class="form-control @error('contact') is-invalid @enderror" id="contact" name="contact" placeholder="Entrer un nom et un prénom..." value="@if (old('contact')) {{ old('contact') }} @else {{$entreprise->contact}} @endif">
                  @error('contact')
                      <span id="name-error" class="error invalid-feedback">{{$errors->first('contact')}}</span>
                  @enderror
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                  <label for="telephone">Téléphone </label>
                  <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone" name="telephone" placeholder="Entrer un numéro de téléphone" value="@if (old('telephone')) {{ old('telephone') }} @else {{$entreprise->telephone}} @endif" data-inputmask="'mask': '99-99-99-99-99'">
                  @error('telephone')
                      <span id="name-error" class="error invalid-feedback">{{$errors->first('telephone')}}</span>
                  @enderror
              </div>
            </div>
        </div>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Modifier l'entreprise</button>
      </div>
    </form>
    </div>

    @section('script')

    <script>

        $("#telephone").inputmask({"mask": "99-99-99-99-99"});
        $("#code_postal").inputmask({"mask": "99999"});

    </script>


    @endsection

@endsection