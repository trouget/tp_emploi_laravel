@extends('layouts.template')
@section('title_page', 'Entreprises')
@section('content')
      <div class="card card-olive">
        <div class="card-header">
          <h3 class="card-title">Liste des entreprises</h3>
        </div>
        <div class="card-body">
            <table id="table" class="table table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Nom</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Adresse</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">contact</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Addresse Email</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Téléphone</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Nombre d'offre</th>
                    <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($entreprises as $entreprise)
                <tr role="row" class="odd">                
                    <td>{{$entreprise->nom}}</td>
                    <td>{{$entreprise->addresse." ".$entreprise->code_postal." ".$entreprise->ville}} </td>
                    <td>{{$entreprise->contact}}</td>
                    <td>{{$entreprise->email}}</td>
                    <td>{{$entreprise->telephone}}</td>
                    <td>{{$entreprise->offres->count()}}</td>
                    <td>
                        <div class="btn-group" style="display: block;text-align: right;">
                            <a href="{{route('entreprises.edit', $entreprise)}}"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                        <button class="btn btn-danger remove-user" data-name="{{$entreprise->nom}}" data-action="{{ route('entreprises.destroy',$entreprise) }}"><i class="fas fa-trash"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @section('script')

      <script>
$("body").on("click",".remove-user",function(){

var current_object = $(this);
var name = current_object.attr('data-name');

swal.fire({

    title: "Êtes vous sur ?",
    text: "Voulez-vous vraiment supprimer l'entreprise suivante : "+name+" ?",
    type: 'warning',
    showCancelButton: true,
    cancelButtonColor: '#6c757d',
    confirmButtonColor: '#dc3545',
    confirmButtonText: 'Supprimer !',
    reverseButtons: true

}).then((result) =>{

    if (result.value) {

        var action = current_object.attr('data-action');

        $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
        $('body').find('.remove-form').append('@csrf');
        $('body').find('.remove-form').append('@method('DELETE')');
        $('body').find('.remove-form').submit();
    }

});

});

      </script>
          
      @endsection

@endsection
