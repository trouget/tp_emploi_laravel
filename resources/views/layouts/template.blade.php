<!DOCTYPE HTML>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>PlatOffres - @yield('title_page')</title>

  <link rel="icon" type="image/ico" href="{{asset('assets/img/logo.ico') }}"/>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

  <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="layout-top-nav layout-navbar-fixed ">
  @include('sweetalert::alert')
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="{{ route('home') }}" class="navbar-brand">
        <img src="{{ asset('assets/img/logo.png') }}" class="brand-image img-circle"
             style="opacity: .8">
        <span class="brand-text font-weight-light">PlatOffres</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link {{ request()->routeIs('home') ? 'active' : ''}}">Acceuil</a>
          </li>
          @if (Auth::user()->is_admin == false)
          <li class="nav-item">
            <a href="{{ route('offres.index')}}" class="nav-link {{ request()->routeIs('offres.index', 'offres.show') ? 'active' : ''}}"><i class="fas fa-briefcase" style="margin-right: 5px;"></i>Offres d'emploi</a>
          </li>
          @else
          <li class="nav-item dropdown">
            <a id="dropdown_offre" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle {{ request()->routeIs('offres.create', 'offres.edit', 'offres.index') ? 'active' : ''}}"><i class="fas fa-briefcase" style="margin-right: 5px;"></i>Offres d'emploi</a>
                <ul aria-labelledby="dropdown_offre" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('offres.index') }}" class="dropdown-item {{ request()->routeIs('offres.index') ? 'active' : ''}}" ><i class="fas fa-list-ul" style="margin-right: 5px;"></i>Liste des offres d'emploi</a></li>
                <li class="dropdown-divider"></li>
                <li><a href="{{ route('offres.create') }}" class="dropdown-item {{ request()->routeIs('offres.create') ? 'active' : ''}}"><i class="fas fa-plus-circle" style="margin-right: 5px;"></i>Ajouter une offre d'emploi</a></li>
                </ul>
            </li>
          <li class="nav-item dropdown">
            <a id="dropdown_entreprise" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle {{ request()->routeIs('entreprises.create', 'entreprises.edit', 'entreprises.index') ? 'active' : ''}}"><i class="fas fa-building" style="margin-right: 5px;"></i>Entreprises</a>
                <ul aria-labelledby="dropdown_entreprise" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('entreprises.index') }}" class="dropdown-item {{ request()->routeIs('entreprises.index') ? 'active' : ''}}" ><i class="fas fa-list-ul" style="margin-right: 5px;"></i>Liste des entreprises</a></li>
                <li class="dropdown-divider"></li>
                <li><a href="{{ route('entreprises.create') }}" class="dropdown-item {{ request()->routeIs('entreprises.create') ? 'active' : ''}}"><i class="fas fa-plus-circle" style="margin-right: 5px;"></i>Ajouter une entreprise</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
            <a id="dropdown_users" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle {{ request()->routeIs('users.create', 'users.edit', 'users.index') ? 'active' : ''}}"><i class="fas fa-users" style="margin-right: 5px;"></i>Utilisateurs</a>
                <ul aria-labelledby="dropdown_users" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('users.index') }}" class="dropdown-item {{ request()->routeIs('users.index') ? 'active' : ''}}" ><i class="fas fa-list" style="margin-right: 5px;"></i>Liste des utilisateurs</a></li>
                <li class="dropdown-divider"></li>
                <li><a href="{{ route('users.create') }}" class="dropdown-item {{ request()->routeIs('users.create') ? 'active' : ''}}"><i class="fas fa-user-plus" style="margin-right: 5px;"></i>Ajouter un utilisateur</a></li>
                </ul>
            </li>
              <!-- End Level two -->
            </ul>
          </li>
          @endif
        </ul>

      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown user-menu {{ (request()->routeIs('users.show')) || ((request()->routeIs('users.edit')) && (request()->route('user') == Auth::user())) ? 'active' : ''}}">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <i class="nav-icon fas fa-user-alt fa-lg" style="margin-right: 10px;"></i>
            <span class="d-none d-md-inline">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <!-- User image -->
              <li class="user-header bg-secondary">
                <i class="nav-icon fas fa-user-alt fa-4x"></i>
                <p style="margin-top: 20px">
                  {{Auth::user()->name}}
                <small>{{Auth::user()->email}}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <a href="{{ route('users.show', Auth::user()) }}"><button type="button" class="btn btn-block btn-primary" style="width: auto;display: inline;">Profil<i class="fas fa-id-card" style="margin-left: 10px;"></i></button></a>
                <form method="POST" action="{{ route('logout') }}" style="display: inline;">
                    @csrf
                    @method('POST')
                    <button type="submit" class="btn btn-block btn-danger float-right" style="width: auto;">Déconnexion <i class="fas fa-power-off" style="margin-left: 5px;"></i></button>                    
                </form>
              </li>
            </ul>
          </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="content-header">
        <div class="container">
       
        </div><!-- /.container-fluid -->
      </div>

     
    <!-- Main content -->
    <div class="content">
      @yield('menu')
      <div class="container">
          @yield('content')

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline" style="text-align: end;">
     <a href="http://pasteurmontroland.com/"> <img src="{{ asset('assets/img/logo_pasteur.png') }}" style="width: 15%;height: 20%;margin-right: 5px;">Lycée Pasteur Mont Roland </a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020-2021 Loïc OUTHIER</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>

<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>

<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ asset('assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

@yield('script')
<script type="text/javascript">

$(function () {
    $("#table").DataTable();

    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
});

$(document).ready(function () {
  bsCustomFileInput.init();
});



</script>

</body>
</html>
