<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('offres', function (Blueprint $table) {
            $table->id('id_offre');
            $table->string('titre');
            $table->longText('description');
            $table->string('niveau');
            $table->integer('id_entreprise')->index('FK_id_entreprise');
            $table->string('pdf')->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
