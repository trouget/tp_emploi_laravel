<?php

use Illuminate\Database\Seeder;
use \App\Models\User; 

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        $user = new User;
        $user->name = 'Hugo BUFFARD';
        $user->email = 'h.buffard@gmail.com';
        $user->password = \Hash::make('testtest');
        $user->is_admin = 0;
        $user->save();

        $user2 = new User;
        $user2->name = 'Loïc OUTHIER';
        $user2->email = 'loic.outhier.work@gmail.com';
        $user2->password = \Hash::make('rootroot');
        $user2->is_admin = 1;
        $user2->save();

        $user3 = new User;
        $user3->name = 'Sébastien PERNELLE';
        $user3->email = 'spernelle@gmail.com';
        $user3->password = \Hash::make('spernelle');
        $user3->is_admin = 1;
        $user3->save();

        $user4 = new User;
        $user4->name = 'Julian COURBEZ';
        $user4->email = 'julian.courbez@gmail.com';
        $user4->password = \Hash::make('jcourbez');
        $user4->is_admin = 1;
        $user4->save();

        $user5 = new User;
        $user5->name = 'Théo ROUGET';
        $user5->email = 'trougetgmail.com';
        $user5->password = \Hash::make('trouget');
        $user5->is_admin = 0;
        $user5->save();

    }
}
